# Materials and methods

The data was analyzed using the [New tuxedo pipeline](http://www.nature.com/doifinder/10.1038/nprot.2016.095). 

## Data download
The genomes of *Drosophila melanogaster* and *Drosophila simulans* were downloaded from [flybase](http://flybase.org/) along with the GTF annotations.

```
# *Drosophila melanogaster*
wget ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r6.16_FB2017_03/fasta/dmel-all-chromosome-r6.16.fasta.gz
wget ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r6.16_FB2017_03/gtf/dmel-all-r6.16.gtf.gz

# *Drosophila simulans*
wget ftp://ftp.flybase.net/releases/FB2017_03/dsim_r2.02/fasta/dsim-all-chromosome-r2.02.fasta.gz
wget ftp://ftp.flybase.net/releases/FB2017_03/dsim_r2.02/gtf/dsim-all-r2.02.gtf.gz
```

## Data alignement

The alignement of all samples on their corresponding reference genome was performed with [Hisat2](https://www.nature.com/articles/s41587-019-0201-4) (`version 2.1.0`) using following command line options:

```{bash}
hisat2-build $Genome $GenomeIndex
hisat2 -p 7 -k 2 --dta -x $GenomeIndex -1 $Sample_1.fq.gz -2 $Sample_2.fq.gz -S $Sample.sam 
```

We use `--dta` as we will perform an assembly of the transcript. We also used `-k 2` to report a maximum of 2 alignments per sequencing read. As we want to exclude potential expressed transposable element from the assembly, we will discard multimapped reads using the following command line: 

```{bash}
samtools view -h -@ 7 -f 0x2 $Sample.bam |awk '{if(substr($1, 0, 1)=="@" || $5==60){print $0}}' >$Sample.sam
samtools view -h -@ 7 -b -S $Sample.sam >$Sample_filtered.bam && samtools sort -@ 7 $Sample_filtered.bam sorted.bam && mv sorted.bam $Sample_filtered.bam
```

The option `-f 0x2` extracts only the properly aligned paired reads from the bam, and the the `awk` sequence keeping the header part and the reads with a `MAPQ=60`. In `Hisat2` a `MAPQ=60` means that the read was uniquely aligned regardless of the mismatches / indels number. The `BAM` file is also sorted.

At this stage, reads were aligned on the reference genome, and we kept only properly and uniquely mapped reads. This is a stringent approach used to discard transposable elements from the analysis.


## Transcripts assembly

We used `StringTie` to assemble the transcripts using the genomic coordinates of the aligned reads for each sample:

```{bash}
stringtie -p 7 -o $Sample_assemb.gtf -j 2 $Sample_filtered.bam
echo "$Sample$_assemb.gtf" >> mergelist.txt
```
The `-j` option was used to say that at least 2 reads need to support an exon-exon junction and create a new transcript. The default values used by `StringTie` is 1 and we wanted to be more stringent.

One assembly per sample is created, and in order to merge these assemblies, we used the following command line:
```{bash}
stringtie --merge -o merged.gtf mergelist.txt
```

## Transcript expression quantification

We used `Stringtie` to quantify transcript expression in each sample:

```{bash}
stringtie -e -B -p 7 -G merged.gtf -o $Sample/$Sample.gtf $Sample_filtered.bam
```

`-e` option is used to estimate the expression of transcripts of the `GTF` file given to `-G` option. `-B` option is used to create an output needed to run `ballgown` later.

## Count matrix generation

We used `prepDE.py` script from [`StringTie`](https://github.com/gpertea/stringtie/blob/master/prepDE.py) to generate the count matrix from the output of `StringTie`.

## Gene expression level analysis

We used a `R` package called [`ballgown`](https://www.nature.com/articles/nbt.3172) to calculate the TPM expression of each gene or transcript. Data are loaded with `ballgown` from the output of `StringTie`. Transcript with low expression are filtered out as recommended in [this paper](http://www.nature.com/doifinder/10.1038/nprot.2016.095) using :

```{R}
subset(data,"rowMeans(texpr(data))>0.5",genomesubset=TRUE)
``` 

FPKM were then calculated using texpr function from ballgown. TPM where then calculated from this table by normalizing the sum of each sample to 1e6. The belefit of TPM over FPKM is explained [here](https://link.springer.com/article/10.1007/s12064-012-0162-3).

https://haroldpimentel.wordpress.com/2014/05/08/what-the-fpkm-a-review-rna-seq-expression-units/

## Gene annotation

Genes and transcripts reconstructed by `StringTie` will be compared to the known annotation of the genome using [`gffcompare`](https://ccb.jhu.edu/software/stringtie/gffcompare.shtml). Each new transcript will be assigned a class code described in `gffcompare` documentation depending on its similarity to reference transcript.
